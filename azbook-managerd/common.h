/* common.h -- Azbook manager common definitions.
 *
 * Copyright (C) 2018-2020 "AZ Company Group" LLC <https://gkaz.ru/>
 * Copyright (C) 2018-2020 Artyom V. Poptsov <a@gkaz.ru>
 *
 * This file is part of Azbook.
 *
 * Azbook is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Azbook is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Azbook.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __COMMON_H__
#define __COMMON_H__

/* #define P7_SHARED       TM("azbook-managerd:p7") */
/* #define P7_SHARED_TRACE TM("azbook-managerd:tr") */

#define P7_SHARED       TM("azbook:p7")
#define P7_SHARED_TRACE TM("azbook:tr")


extern const char* AZBOOK_MANAGER_SERVICE_NAME;
extern const char* AZBOOK_MANAGER_SERVICE_PATH;

#endif /* ifndef __COMMON_H__ */

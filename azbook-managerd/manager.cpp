/* manager.cpp -- Azbook manager implementation.
 *
 * Copyright (C) 2018-2020 "AZ Company Group" LLC <https://gkaz.ru/>
 * Copyright (C) 2018-2020 Artyom V. Poptsov <a@gkaz.ru>
 *
 * This file is part of Azbook.
 *
 * Azbook is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Azbook is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Azbook.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <dbus-c++/dbus.h>
#include <sys/stat.h>

#include "manager.hpp"
#include "common.h"

#include "config.h"
#include "logger-client.hpp"


//// Constants

const char* AZBOOK_MANAGER_SERVICE_NAME = "ru.gkaz.Azbook.Manager";
const char* AZBOOK_MANAGER_SERVICE_PATH = "/ru/gkaz/Azbook/Manager";

static const char* AZBOOK_MANAGER_MODULE = "azbook manager";

static SCM _lookup(const char* proc)
{
    return scm_c_public_lookup(AZBOOK_MANAGER_MODULE, proc);
}


/**
 * The main constructor.
 *
 * @param connection DBus connection.
 * @param config Azbook configuration.
 * @throws runtime_error
 */
Manager::Manager(DBus::Connection& connection, const string& config_file)
     : DBus::ObjectAdaptor(connection, AZBOOK_MANAGER_SERVICE_PATH),
       Logger_client("manager")
{
     configure_logging();
     this->make_manager             = _lookup("make-manager");
     this->manager_session_active_p = _lookup("manager-session-active?");
     this->manager_session_start_x  = _lookup("manager-session-start!");
     this->manager_session_stop_x   = _lookup("manager-session-stop!");
     this->manager_authenticate_x   = _lookup("manager-authenticate!");
     this->manager_sessions_list    = _lookup("manager-sessions-list");
     this->manager_users_list       = _lookup("manager-users-list");
     this->manager_delete_user_x    = _lookup("manager-delete-user!");
     this->manager = scm_call_1(scm_variable_ref(this->make_manager),
                                scm_from_locale_string(config_file.c_str()));
     umask(S_IRGRP | S_IWGRP | S_IWOTH | S_IROTH);
     LOG_TRACE("%s", "Manager created");
}

int32_t Manager::Authenticate(const string& login, const string& password) {
    SCM result = scm_call_3(scm_variable_ref(this->manager_authenticate_x),
                            this->manager,
                            scm_from_locale_string(login.c_str()),
                            scm_from_locale_string(password.c_str()));
    return scm_is_true(result);
}

/**
 * Handle DBus 'StartSession' request.
 *
 * @param login User login.
 * @param password User password.
 */
int32_t Manager::StartSession(const string& login)
{
    SCM result = scm_call_2(scm_variable_ref(this->manager_session_active_p),
                            this->manager,
                            scm_from_locale_string(login.c_str()));
    if (! scm_is_true(result)) {
        LOG_WARNING("User %s is not authenticated", login.c_str());
        return false;
    }

    result = scm_call_2(scm_variable_ref(this->manager_session_start_x),
                        this->manager,
                        scm_from_locale_string(login.c_str()));
    return scm_is_true(result);
}

int32_t Manager::EndSession(const string& login)
{
    SCM result = scm_call_2(scm_variable_ref(this->manager_session_stop_x),
                            this->manager,
                            scm_from_locale_string(login.c_str()));
    return scm_is_true(result);
}

int32_t Manager::DeleteUser(const string& fqn)
{
     SCM result = scm_call_2(scm_variable_ref(this->manager_delete_user_x),
                             this->manager,
                             scm_from_locale_string(fqn.c_str()));
     return scm_is_true(result);
}

vector<string> Manager::ListSessions()
{
     vector<string> result;
     SCM list = scm_call_1(scm_variable_ref(this->manager_sessions_list),
                           this->manager);
     int32_t length = scm_to_int(scm_length(list));
     for (int idx = 0; idx < length; idx++) {
         char* login = scm_to_locale_string(scm_list_ref(list, scm_from_int(idx)));
         result.push_back(string(login));
     }

     return result;
}

vector<string> Manager::ListUsers()
{
     vector<string> result;
     SCM list = scm_call_1(scm_variable_ref(this->manager_users_list),
                           this->manager);
     int32_t length = scm_to_int(scm_length(list));
     for (int idx = 0; idx < length; idx++) {
         char* login = scm_to_locale_string(scm_list_ref(list, scm_from_int(idx)));
         result.push_back(string(login));
     }

     return result;
}


string Manager::SessionStatus(const string& login)
{
    return "nothing";
}

//// Manager.cpp ends here.

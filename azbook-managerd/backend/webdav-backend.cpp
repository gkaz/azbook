#include <memory>
#include "webdav.h"
#include "webdav-backend.hpp"
#include "config.h"
#include "logger-client.hpp"
#include "backend.hpp"

using namespace std;

static const string COOKIE_JAR_FILE_PREFIX = "/etc/azbook.d/cookie-";

WebDAV_backend::WebDAV_backend(shared_ptr<Config> config)
    : Backend(config)
{
    // Do nothing.
}

bool WebDAV_backend::login(const string& login,
                           const string& password)
{
    try {
        Backend::config->put("user_name", login);
        WebDAV webdav(config->get("webdav_url"),
                      login,
                      password,
                      config->get("ca_path"),
                      false);
        string cookie_jar_file = COOKIE_JAR_FILE_PREFIX + login;
        Backend::config->put("session_cookie", cookie_jar_file);
        LOG_TRACE("%s", "Performing authentication ...");
        CURLcode result = webdav.authenticate(cookie_jar_file);
        LOG_TRACE("Authentication result: %d", result);
        if ((result >= 200) && (result < 300)) {
            return true;
        } else {
            LOG_CRITICAL("Authentication error.  HTTP response: %d", result);
            return false;
        }
    } catch (out_of_range& e) {
        LOG_CRITICAL("Authentication error: %d", e.what());
        return false;
    }
}

bool WebDAV_backend::logout()
{
    string login = Backend::config->get("user_name");
    string cookie_jar_file = COOKIE_JAR_FILE_PREFIX + login;

    if (remove(cookie_jar_file.c_str())) {
        LOG_CRITICAL("Could not remove a cookie jar for user '%s': %s",
                     login.c_str(),
                     cookie_jar_file.c_str());
        throw runtime_error("Could not remove user cookie jar: "
                            + cookie_jar_file);
        return false;
    }

    if (remove(Backend::config->get_file_name().c_str())) {
        LOG_CRITICAL("Could not remove the config for user '%s': %s",
                     login.c_str(),
                     Backend::config->get_file_name().c_str());
        return false;
    }

    return true;
}

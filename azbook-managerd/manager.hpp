/* manager.hpp -- Azbook manager header.
 *
 * Copyright (C) 2018-2020 "AZ Company Group" LLC <https://gkaz.ru/>
 * Copyright (C) 2018-2020 Artyom V. Poptsov <a@gkaz.ru>
 *
 * This file is part of Azbook.
 *
 * Azbook is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Azbook is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Azbook.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MANAGER_HPP__
#define __MANAGER_HPP__

#include <unordered_map>

#include <dbus-c++/dbus.h>
#include <libguile.h>

#include "Manager_adaptor.hpp"
#include "libazbook-common/config.h"
#include "logger-client.hpp"

using namespace std;


/**
 * Manager exception.
 */
class Manager_exception : public runtime_error {
public:
     Manager_exception(const string& what)
          : runtime_error(what)
          {
          }
};


class Manager
     : public ru::gkaz::Azbook::Manager_adaptor,
       public DBus::IntrospectableAdaptor,
       public DBus::ObjectAdaptor,
       Logger_client
{
public:
     Manager(DBus::Connection& connection, const string& config_file);

     int32_t Authenticate(const string& login, const string& password);
     int32_t StartSession(const string& login);
     int32_t EndSession(const string& login);
     vector<string> ListSessions();
     vector<string> ListUsers();
     int32_t DeleteUser(const string& fqn);
     string SessionStatus(const string& login);

private:
     // Azbook Manager instance.
     SCM manager;

     // SCM procedures.
     SCM make_manager;
     SCM manager_authenticate_x;
     SCM manager_session_active_p;
     SCM manager_session_start_x;
     SCM manager_session_stop_x;
     SCM manager_sessions_list;
     SCM manager_users_list;
     SCM manager_delete_user_x;
};

#endif	// ifndef __MANAGER_HPP__

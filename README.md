# Dependencies
* libxml++ >= 2.6
* libcurl
* libpam0
* libdbus-c++
* guile
* guile-library

## Build-time
* Doxygen (needed only to produce the Doxgen documentation)
* git
* gcc-c++
* autotools
* automake
* libtool
* cmake

## Distro-specific deps
### ALT Linux 10
```
$ sudo apt-get install \
    make \
    cmake \
    git \
    gcc-c++ \
    automake \
    autoconf \
    libtool \
    libdbus-c++-devel \
    libcurl-devel \
    libpam0-devel \
    libxml++3-devel
$ cd azbook
$ make install-guile-lib
```

### ALT Linux 9
```
$ sudo apt-get install \
    make \
    git \
    gcc-c++ \
    automake \
    autoconf \
    libtool \
    libdbus-c++-devel \
    libcurl-devel \
    libpam0-devel
$ cd azbook
$ make install-guile-lib
```

### Ubuntu
```
$ sudo apt install \
    cmake \
    make  \
    git \
    g++ \
    automake \
    autoconf \
    libtool \
    libdbus-c++-dev \
    libdbus-1-dev \
    libcurl4-openssl-dev \
    libpam0g-dev \
    libxml++2.6-dev \
    guile-library
```

### Debian
* wget
* libdbus-c++-dev
* libdbus-1-dev

# Building
You can get the sources as the follows:
```
$ git clone https://gitlab.com/gkaz/azbook.git
$ cd azbook
$ git submodule init
$ git submodule update
```

Azbook uses GNU Autotools so the building process is quite simple:
```
$ autoreconf -vif
$ ./configure --prefix=/usr --with-libsecuritydir=/lib64/security/
$ make -j$(nproc)
```

To install Azbook to the system, issue the following commands:
```
$ sudo make install
$ sudo systemctl enable azbook-managerd
```

# Documentation
There's auto-generated Doxygen documentation.  To produce the documentation, run:
```
$ make docs
```

The docs will be saved to `html` directory.

# Usage
## Tested WebDAV servers
* Azbook Server
* ownCloud
* NextCloud

## azbookd
This is the synchronization daemon. Usually it is started by the
`azbook-managerd` daemon.

It can be started manually as follows:
```
$ ./azbookd [options] <directory-to-watch>
```

Possible options:
* `--help`, `-h` -- Show program help message.
* `--daemon`, `-D` -- Run the program as daemon.
* `--config <config>`, `-C <config>` -- Set the configuration file path.

## azbook-managerd
Azbook session manager that handles PAM authentication requests and starts the
Azbook sessions for authenticated users.

# Configuration

## PAM

To enable Azbook PAM integration on Alt Education 10 you must modify
`/etc/pam.d/system-auth-local-only` as the following:

```
#%PAM-1.0
auth		sufficient	pam_tcb.so shadow fork nullok
auth		sufficient	libazbook-pam.so
auth		required	pam_deny.so
account		sufficient	pam_tcb.so shadow fork
account         sufficient	libazbook-pam.so
account		required	pam_permit.so
password	sufficient	pam_passwdqc.so config=/etc/passwdqc.conf
password	sufficient	pam_tcb.so use_authtok shadow fork nullok write_to=tcb
password	sufficient	libazbook-pam.so
session         sufficient	libazbook-pam.so
session		sufficient	pam_tcb.so
session		required	pam_permit.so
```

## Azbook configuration
The configuration file must be stored as `/etc/azbook`.

Configuration files for each user is stored in `/etc/azbook.d/`
directory.

The Azbook configuration allows you to perform variable substitutions in the
configuration; to substitute a variable in an another variable value, add
`${variable_name}` to the value string.

### NextCloud
```
backend:nextcloud
webdav_server:cloud.example.org
webdav_url:https://${webdav_server}/remote.php/dav/files/${user_name}/
ca_path:
file_size_max:01
load_avg_threshold:0.4
logging_configuration:/P7.Sink=FileTxt /P7.Dir=/tmp
synchronization_path:/
log_level:info
```

### Generic
```
backend:webdav
webdav_server:example.org
webdav_url:https://${webdav_server}/files/${user_name}/
ca_path:
file_size_max:01
load_avg_threshold:0.4
logging_configuration:/P7.Sink=FileTxt /P7.Dir=/tmp
synchronization_path:/
log_level:info
```

## Configuration options
Here's a detailed description of options:

### `backend`
WebDAV backend to use.  Currently only two variants are supported:
`nextcloud` and `webdav`.

### `user_name`
Name of the user.

### `user_password`
Plain-text password of the user.  It is not recommended for obvious reasons.

### `webdav_server`
WebDAV server API address or name.

### `webdav_url`
URL of the server.

### `ca_path`
Path to a public part of the server cryptographic certificate.  Useful when a
server uses a self-signed TLS certificate.

### `file_size_max`
Synchronization of files that are bigger than this value will be postponed if
the system load is bigger than `load_avg_threshold`.

Accepted formats include plain numbers of bytes and numbers with one of the
modifiers: `K`(kibibytes), `M` (mebibytes), `G`(gibibytes).  For example: `10M`
means 10 mebibytes.

### `load_avg_threshold`
If the system load is greater than this value, synchronization of big files
files will be postponed.

### `logging_configuration`
Logging configuration for P7 Baical.

### `synchronization_path`
The root directory that the daemon must synchronize.  This option is only needed
for the Azbook manager.

### `log_level`
Level of the logging.  Allowed values are:
- trace
- debug
- info
- warning
- error
- critical

# Development

## Coding standards (WIP)

* https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines


;;; config.scm -- Azbook configuration parser.

;; Copyright (C) 2022 "AZ Company Group" LLC <https://gkaz.ru/>
;; Copyright (C) 2022 Artyom V. Poptsov <a@gkaz.ru>
;;
;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; This module contains the implementation of Azbook configuration parser.


;;; Code:

(define-module (azbook core config)
  #:use-module (oop goops)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 format)
  #:use-module (azbook core common)
  #:export (<config>
            config?
	          config-data
            config-copy
            config-save!
	          config-file-name
            config-file-name-set!
	          config-set!
	          config-ref))


(define-class <config> ()
  ;; <hash-table>
  (data
   #:init-thunk (lambda () (make-hash-table))
   #:getter     config-data)

  ;; <string>
  (file-name
   #:init-value #f
   #:getter     config-file-name
   #:setter     config-file-name-set!))



(define-method (config? x)
  (is-a? x <config>))


(define-method (parse-file (file-name <string>))
  "Parse a FILE-NAME and load configuration from it. Return configuration as a
hash table."
  (let ((result (make-hash-table 5)))
    (let loop ((port (open-input-file file-name)))
      (let ((line (read-line port)))
        (unless (eof-object? line)
          (let* ((kv (string-split line #\:))
                 (k  (car kv))
                 (v  (let ((v (cdr kv)))
                       (if (= (length v) 1)
                           (car v)
                           (string-join v ":")))))
            (hash-set! result k v)
            (loop port)))))
    result))

(define-method (config-copy (config <config>))
  "Make and return an independent copy of a CONFIG."
  (let ((new-config (make <config>)))
    (config-file-name-set! new-config (config-file-name config))
    (hash-map->list (lambda (key value)
                      (format #t "~a: ~a~%" key value)
                      (hash-set! (config-data new-config)
                                 key
                                 value))
                    (config-data config))
    new-config))


(define-method (initialize (config <config>) initargs)
  (next-method)
  (let ((file-name (and (memq #:file-name initargs)
                        (cadr (memq #:file-name initargs)))))
    (when file-name
      (config-file-name-set! config file-name)
      (slot-set! config 'data (parse-file file-name)))))



(define-method (%display (config <config>) (port <port>))
  (format port "#<config file: ~a ~a>"
          (config-file-name config)
          (object-address->hex-string config)))

(define-method (display (config <config>) (port <port>))
  (%display config port))

(define-method (write (config <config>) (port <port>))
  (%display config port))


(define-method (config-set! (config <config>)
			                      key
			                      value)
  "Set an option with a KEY to a VALUE in the CONFIG."
  (hash-set! (config-data config) key value))

(define-method (config-ref (config <config>) key)
  "Get a value for a KEY option from the CONFIG."
  (hash-ref (config-data config) key))

(define-method (config-save! (config <config>))
  (let ((port (open-output-file (config-file-name config))))
    (hash-map->list (lambda (key value)
                      (format port "~a:~a~%" key value))
                    (config-data config))
    (close port)))

(define-method (config-save! (config <config>) (file-name <string>))
  (config-file-name-set! config file-name)
  (config-save! config))

;;; config.scm ends here.

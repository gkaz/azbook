;;; fqn.scm -- Azbook Fully-Qualified Name (FQN.)

;; Copyright (C) 2022 "AZ Company Group" LLC <https://gkaz.ru/>
;; Copyright (C) 2022 Artyom V. Poptsov <a@gkaz.ru>
;;
;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; This module contains implementation of Azbook Fully-Qualified Names
;; (FQN.)


;;; Code:

(define-module (azbook core fqn)
  #:use-module (oop goops)
  #:use-module (ice-9 regex)
  #:use-module (azbook core common)
  #:export(<fqn>
           fqn?
           fqn-login
           fqn-realm
           fqn-valid?
           string->fqn
           fqn->string))


(define %fqn-separator
  #\@)


;; This class defined an Azbook Fully-Qualified Name (FQN).
(define-class <fqn> ()
  ;; <string>
  (login
   #:init-value   #f
   #:init-keyword #:login
   #:getter       fqn-login)

  ;; <string>
  (realm
   #:init-value   #f
   #:init-keyword #:realm
   #:getter       fqn-realm))

(define (fqn? x)
  "Check if X is a <fqn> instance."
  (is-a? x <fqn>))


(define-method (%display (fqn <fqn>) (port <port>))
  (format port "#<fqn ~a~a~a ~a>"
          (fqn-login fqn)
          %fqn-separator
          (fqn-realm fqn)
          (object-address->hex-string fqn)))

(define-method (display (fqn <fqn>) (port <port>))
  (%display fqn port))

(define-method (write (fqn <fqn>) (port <port>))
  (%display fqn port))


(define-method (string->fqn (fqn-string <string>))
  "Convert an FQN-STRING to an <fqn> instance.  Throw 'fqn-error on
errors."
  (let ((lst (string-split fqn-string %fqn-separator)))
    (unless (= (length lst) 2)
      (throw 'fqn-error "Could not parse FQN" fqn-string))
    (make <fqn>
      #:login (car lst)
      #:realm (cadr lst))))

(define-method (fqn->string (fqn <fqn>))
  (string-append (fqn-login fqn)
                 (string %fqn-separator)
                 (fqn-realm fqn)))

(define-method (fqn-valid? (fqn-string <string>))
  "Check if an FQN-STRING is valid, return #t if it is, #f otherwise."
  (if (string-match "^[^@]+@[^@]+$" fqn-string)
      #t
      #f))

;; fqn.scm ends here.

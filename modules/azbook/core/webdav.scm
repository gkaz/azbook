;; Copyright (C) 2021-2022 "AZ Company Group" LLC <https://gkaz.ru/>
;; Copyright (C) 2021-2022 Artyom V. Poptsov <a@gkaz.ru>
;;
;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.

(define-module (azbook core webdav)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-8)
  #:use-module (azbook core base64)
  #:use-module (rnrs bytevectors)
  #:use-module (oop goops)
  #:use-module (web client)
  #:use-module (web uri)
  #:use-module (web response)
  #:use-module (azbook core common)
  #:use-module (azbook core log)
  #:export (<webdav>
            webdav?
            uri-parameters->string
            webdav-build-uri
            webdav-propfind
            webdav-mkcol
            build-auth-header/basic
            build-auth-header/bearer))


(define-class <webdav> ()
  (server-uri
   #:init-value   #f
   #:init-keyword #:server
   #:getter       webdav-server-uri)

  ;; <string>
  (user-name
   #:init-value   #f
   #:init-keyword #:user-name
   #:getter       webdav-user-name)

  ;; <string>
  (user-password
   #:init-value   #f
   #:init-keyword #:user-password
   #:getter       webdav-user-password)

  ;; Known types:
  ;;   'basic, 'bearer
  ;;
  ;; <symbol>
  (auth-type
   #:init-value   'basic
   #:init-keyword #:auth-type
   #:getter       webdav-auth-type))



(define-method (%display (webdav <webdav>) (port <port>))
  (format port "#<webdav ~a ~a>"
          (webdav-server-uri webdav)
          (object-address->hex-string webdav)))

(define-method (display (webdav <webdav>) (port <port>))
  (%display webdav port))

(define-method (write (webdav <webdav>) (port <port>))
  (%display webdav port))



(define-method (webdav? object)
  (is-a? object <webdav>))


(define-method (uri-parameters->string (params <list>))
  (if (null? params)
      ""
      (string-join (map (lambda (p)
                          (string-append (car p) "=" (cdr p)))
                        params)
                   "&")))

(define-method (build-auth-header/basic user-name user-password)
  (let ((creds (format #f "~a:~a" user-name user-password)))
    (cons 'Authorization
          (format #f "Basic ~a" (base64-encode (string->utf8 creds))))))

(define-method (build-auth-header/bearer user-token)
  (cons 'Authorization
        (format #f "Bearer ~a" user-token)))

(define-method (webdav-build-auth-header (webdav <webdav>))
  (let ((user-name     (webdav-user-name webdav))
        (user-password (webdav-user-password webdav)))
    (case (webdav-auth-type webdav)
      ((basic)
       (build-auth-header/basic user-name user-password))
      ((bearer)
       (build-auth-header/bearer user-password))
      (else
       (log-error "Unknown auth type: ~a" (webdav-auth-type webdav))
       (error "Unknown auth type" (webdav-auth-type webdav))))))

(define-method (webdav-build-uri (webdav <webdav>)
                                 (resource <string>)
                                 (query <list>))
  (let ((server (webdav-server-uri webdav)))
    (build-uri (uri-scheme server)
               #:host   (uri-host server)
               #:port   (uri-port server)
               #:path   resource
               #:query  (uri-parameters->string query))))

(define-method (webdav-build-uri (webdav <webdav>)
                                 (resource <string>))
  (webdav-build-uri webdav resource '()))


;; (define-method (webdav-auth (webdav <webdav>)
;;                             (cooke-jar <string>))
;;   #f)

;; (define-method (webdav-upload (webdav <webdav>)
;;                               (path <string>)
;;                               (file-name <string>))
;;   #f)

;; (define-method (webdav-get (webdav <webdav>)
;;                            (path <string>)
;;                            (port <port>))
;;   #f)

(define-method (webdav-delete (webdav <webdav>)
                              (path <string>))
  "Perform a DELETE WebDAV request on the given PATH.  Return two
values: the response and the reponse body."
  (let ((uri (webdav-build-uri webdav path)))
    (receive (response body)
        (http-request uri
                      #:method 'DELETE
                      #:headers (list (webdav-build-auth-header webdav)))
      (values response body))))

(define-method (webdav-mkcol (webdav <webdav>)
                             (path <string>))
  "Perform a MKCOL WebDAV request on the given PATH.  Return two
values: the response and the reponse body."
  (let ((uri (webdav-build-uri webdav path)))
    (receive (response body)
        (http-request uri
                      #:method 'MKCOL
                      #:headers (list (webdav-build-auth-header webdav)))
      (values response body))))

(define-method (webdav-propfind (webdav <webdav>)
                                (path <string>))
  "Perform a PROPFIND WebDAV request on the given PATH.  Return two
values: the response and the response body."
  (let ((uri (webdav-build-uri webdav path)))
    (receive (response body)
        (http-request uri
                      #:method 'PROPFIND
                      #:headers (list (webdav-build-auth-header webdav)))
      (values response body))))

;; (define-method (webdav-resource-exists? (webdav <webdav>)
;;                                         (resource <string>))
;;   #f)

;; (define-method (webdav-server-available? (webdav <webdav>))
;;   #f)

;;; webdav.scm ends here.

;;; common.scm -- Azbook common procedures.

;; Copyright (C) 2022 "AZ Company Group" LLC <https://gkaz.ru/>
;; Copyright (C) 2022 Artyom V. Poptsov <a@gkaz.ru>
;;
;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; This module contains Azbook common procedures.


;;; Code:

(define-module (azbook core common)
  #:export (object-address->hex-string
            define-class-with-docs
            define-macro-with-docs
            define-with-docs
            define-generic-with-docs))


(define (object-address->hex-string object)
  "Convert the address of an OBJECT to a HEX string."
  (number->string (object-address object) 16))


;;; This part is taken from (scheme documentation). The code is licensed under
;;; GNU GPL v3.0+ license.
;; Copyright (C) 2003,2004  Andy Wingo <wingo at pobox dot com>

(define-macro (define-macro-with-docs name-and-args docs . body)
  "Define a macro with documentation."
  `(define-macro ,name-and-args ,docs ,@body))

(define-macro-with-docs (define-with-docs sym docs val)
  "Define a variable with documentation."
  `(begin
     (define ,sym ,val)
     (set-object-property! ,sym 'documentation ,docs)
     *unspecified*))

(define-macro-with-docs (define-generic-with-docs name documentation)
  "Define a generic function with documentation."
  `(define-with-docs ,name ,documentation
     (make-generic ',name)))

(define-macro-with-docs (define-class-with-docs name supers docs . slots)
  "Define a class with documentation."
  `(begin
     (define-class ,name ,supers ,@slots)
     (set-object-property! ,name 'documentation ,docs)
     (if #f #f)))

;;; common.scm ends here.


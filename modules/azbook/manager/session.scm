;;; session.scm -- Azbook manager sessions.

;; Copyright (C) 2022 "AZ Company Group" LLC <https://gkaz.ru/>
;; Copyright (C) 2022 Artyom V. Poptsov <a@gkaz.ru>
;;
;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; This module contains the implementation of the Azbook manager session.


;;; Code:

(define-module (azbook manager session)
  #:use-module (oop goops)
  #:use-module (ice-9 rdelim)
  #:use-module (azbook manager backend nextcloud)
  #:use-module (azbook core common)
  #:use-module (azbook core config)
  #:use-module (azbook core log)
  #:use-module (azbook core fqn)
  #:use-module (azbook config)
  #:export (<session>
            session?
            session-pid
            session-pid-set!
            session-fqn
            session-user-name
            session-user-password
            session-local-user-login
            session-local-user-login-set!
            session-authenticate
            session-config
            session-start!
            session-stop!
            session-save-config!))


(define %pid-directory "/var/run/azbook")

(define %azbookd-binary (string-append
                         %azbook-installation-prefix
                         "/bin/azbookd"))


(define-class <session> ()
  ;; Session PID file that stores azbookd daemon PID.
  ;;
  ;; <string>
  (pid-file
   #:init-value   #f
   #:init-keyword #:pid-file
   #:getter       session-pid-file
   #:setter       session-pid-file-set!)

  ;; <fqn>
  (fqn
   #:init-value   #f
   #:init-keyword #:fqn
   #:getter       session-fqn)

  ;; Local user login that is used on the host system.
  ;;
  ;; <string>
  (local-user-login
   #:init-value   #f
   #:init-keyword #:local-user-login
   #:getter       session-local-user-login
   #:setter       session-local-user-login-set!)

  ;; <string>
  (user-password
   #:init-value   #f
   #:init-keyword #:user-password
   #:getter       session-user-password)

  ;; <backend>
  (backend
   #:init-value   #f
   #:getter       session-backend
   #:setter       session-backend-set!)

  (config
   #:init-value   #f
   #:init-keyword #:config
   #:getter       session-config))


(define (session? x)
  (is-a? x <session>))


(define-method (%display (session <session>) (port <port>))
  (format port "#<session ~a ~a>"
          (fqn->string (session-fqn session))
          (object-address->hex-string session)))

(define-method (display (session <session>) (port <port>))
  (%display session port))

(define-method (write (session <session>) (port <port>))
  (%display session port))


(define-method (session-user-name (session <session>))
  "Get the user name from the SESSION FQN."
  (fqn-login (session-fqn session)))

(define-method (session-user-realm (session <session>))
  "Get the user realm from the SESSION FQN."
  (fqn-realm (session-fqn session)))


(define-method (initialize (session <session>) initargs)
  (next-method)
  (let ((fqn (and (memq #:fqn initargs)
                  (cadr (memq #:fqn initargs)))))

    (unless (fqn? fqn)
      (log-error "Wrong #:fqn parameter type: expected <fqn>, found: ~a"
                 fqn)
      (error "Wrong #:fqn parameter type" fqn))

    (cond
     ((string=? (fqn-realm fqn) "nextcloud")
      (let ((backend (make <backend:nextcloud>
                       #:config (session-config session))))
        (session-backend-set! session backend)))
     (else
      (error "Not supported yet" (session-user-realm session))))))

(define-method (session-authenticate (session <session>))
  (backend-login (session-backend session)
                 (session-user-name session)
                 (session-user-password session)))

(define-method (session-config-sync-path (session <session>))
  "Get the synchronization path for a SESSION."
  (config-ref (session-config session) "synchronization_path"))

(define (user-dbus-socket pw)
  "Get a DBus socket path for a user described by a passwd record PW."
  (format #f "/run/user/~a/bus" (passwd:uid pw)))


(define-method (session-start! (session <session>))
  (let* ((sync-dir         (session-config-sync-path session))
         (local-user-login (session-local-user-login session))
         (pw        (getpwnam local-user-login))
         (sync-path (format #f "/home/~a/~a" local-user-login sync-dir)))
    (log-info "session-start!: User: ~a~%" (session-fqn session))
    (log-info "session-start!: azbookd binary: ~a~%" %azbookd-binary)
    (log-info "session-start!: synchronization path: ~a~%" sync-path)

    (log-info "session-start!: Check if PID directory exists: '~a' ..."
              %pid-directory)
    (unless (file-exists? %pid-directory)
      (log-info "session-start!:   Creating '~a'"
              %pid-directory)
      (mkdir %pid-directory))
    (log-info "session-start!: Check if PID directory exists: '~a' ... done"
              %pid-directory)

    (let ((pid-subdir (string-append %pid-directory "/" local-user-login)))
      (log-info "session-start!: PID subdirectory: '~a'" pid-subdir)
      (unless (file-exists? pid-subdir)
        (log-info "session-start!:   Creating PID subdirectory: '~a'"
                  pid-subdir)
        (mkdir pid-subdir)
        (chown pid-subdir (passwd:uid pw) (passwd:gid pw)))
      (log-info "session-start!: User config: ~a"
                (config-file-name (session-config session)))
      (let* ((pid-file (string-append pid-subdir "/azbookd.pid"))
             (pid      (primitive-fork)))
        (log-info "session-start!: PID: ~a" pid)
        (cond
         ((zero? pid)
          (setuid (passwd:uid pw))
          (let ((d-bus (user-dbus-socket pw)))
            (if (file-exists? d-bus)
                (execl %azbookd-binary
                       %azbookd-binary
                       "--config" (config-file-name (session-config session))
                       "--pid-file" pid-file
                       sync-path)
                (execl "/bin/dbus-run-session"
                       "/bin/dbus-run-session"
                       "--"
                       %azbookd-binary
                       "--config" (config-file-name (session-config session))
                       "--pid-file" pid-file
                       sync-path))
            (log-error "session-start!: Could not start the azbookd for '~a'"
                       (session-fqn session))
            (error "Could not start the azbookd"
                   (session-fqn session)
                   local-user-login)))
         ((> pid 0)
          (log-info "session-start!: azbookd PID for '~a' (~a): ~a"
                    (session-fqn session)
                    local-user-login
                    pid)
          (session-pid-file-set! session pid-file))
         (else
          (log-error "session-start!: Could not start a process for '~a'"
                     (session-fqn session)
                     local-user-login)
          (error "Could not start a process"
                 (session-fqn session)
                 local-user-login)))))))


(define-method (session-stop! (session <session>))
  "Stop a SESSION."
  (let ((pid-file (session-pid-file session)))
    (catch #t
      (lambda ()
        (let* ((p        (open-input-file pid-file))
               (pid-line (read-line p)))
          (when (eof-object? pid-line)
            (close p)
            (log-error "Could not read PID from a file: ~a" pid-file)
            (error "Could not read PID from a file" pid-file))
          (let ((pid (string->number pid-line)))
            (kill pid SIGTERM)
            (waitpid pid))))
      (lambda (key . args)
        (log-error "Could not open a PID file: ~a" pid-file))))

  (backend-logout (session-backend session))
  (delete-file (config-file-name (session-config session))))


(define-method (session-save-config! (session <session>) (file-name <string>))
  (config-file-name-set! (session-config session) file-name)
  (config-save! (session-config session))
  (let ((pw (getpwnam (session-local-user-login session))))
    (chown file-name
           (passwd:uid pw)
           (passwd:gid pw))
    (chmod file-name
           #o600)
    (sync)))

;;; session.scm ends here.

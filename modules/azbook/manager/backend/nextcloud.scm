;;; nextcloud.scm -- Azbook NextCloud authentication backend.

;; Copyright (C) 2022 "AZ Company Group" LLC <https://gkaz.ru/>
;; Copyright (C) 2022 Artyom V. Poptsov <a@gkaz.ru>
;;
;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; This module contains Azbook NextCloud authentication backend.


;;; Code:

(define-module (azbook manager backend nextcloud)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-8)
  #:use-module (oop goops)
  #:use-module (web client)
  #:use-module (web uri)
  #:use-module (web response)
  #:use-module (sxml simple)
  #:use-module (azbook core config)
  #:use-module (azbook core log)
  #:use-module (azbook core common)
  #:use-module (azbook core webdav)
  #:use-module (azbook manager backend backend)
  #:re-export (<backend>
               backend-login
               backend-logout)
  #:export (<backend:nextcloud>
            backend:nextcloud?))



(define %getapp-password-resource "/ocs/v2.php/core/getapppassword")
(define %app-password-resource "/ocs/v2.php/core/getapppassword")

(define-class <backend:nextcloud> (<backend>))


(define (backend:nextcloud? x)
  (is-a? x <backend:nextcloud>))



(define-method (%display (backend <backend:nextcloud>) (port <port>))
  (format port "#<backend:nextcloud ~a>"
          (object-address->hex-string backend)))

(define-method (display (backend <backend:nextcloud>) (port <port>))
  (%display backend port))

(define-method (write (backend <backend:nextcloud>) (port <port>))
  (%display backend port))


(define-method (backend-login (backend <backend:nextcloud>)
			      (login <string>)
			      (password <string>))
  (log-info "backend-login: backend: ~a; login: ~a" backend login)
  (let* ((server (build-uri 'https
                            #:host  (config-ref (backend-config backend) "webdav_server")))
         (uri    (build-uri (uri-scheme server)
                            #:host (uri-host server)
                            #:port (uri-port server)
                            #:path %getapp-password-resource)))
    (log-debug "backend-login: backend server: ~a~%"
	      (config-ref (backend-config backend) "webdav_server"))
    (log-debug "backend-login: backend URI: ~a" uri)
    (catch #t
	   (lambda ()
	     (receive (response body)
		 (http-request uri
			       #:method 'GET
			       #:headers (list (cons 'OCS-APIRequest "true")
					       (build-auth-header/basic login password)))
	       (log-debug "backend-login: response: ~a" response)
	       (log-debug "backend-login: response code: ~a" (response-code response))
	       (if (>= (response-code response) 400)
		   (begin
		     (log-error "backend-login: error: response with code ~a"
				(response-code response))
		     #f)
		   (let* ((sxml (xml->sxml body))
			  (ocs  (caddr sxml))
			  (data (list-ref ocs 4))
			  (pass (cadadr (cdr data))))
		     (config-set! (backend-config backend)
				  "user_name"
				  login)
		     (config-set! (backend-config backend)
				  "user_password"
				  pass)
		     #t))))
	   (lambda (key . args)
	     (log-error "Could not execute a request: ~a ~a" key args)
	     #f))))

(define-method (backend-logout (backend <backend:nextcloud>))
  (let* ((server (build-uri 'https
                            #:host  (config-ref (backend-config backend) "webdav_server")))
         (uri    (build-uri (uri-scheme server)
                            #:host (uri-host server)
                            #:port (uri-port server)
                            #:path %app-password-resource)))
    (catch #t
	   (lambda ()
	     (receive (response body)
		 (http-request uri
			       #:method 'DELETE
			       #:headers (list (cons 'OCS-APIRequest "true")
					       (build-auth-header/bearer
						(config-ref (backend-config backend)
							    "user_password"))))
	       #t))
	   (lambda (key . args)
	     (log-error "Could not execute a request: ~a ~a" key args)
	     #f))))

;;; nextcloud.scm ends here.

;;; backend.scm -- Azbook generic authentication backend.

;; Copyright (C) 2022 "AZ Company Group" LLC <https://gkaz.ru/>
;; Copyright (C) 2022 Artyom V. Poptsov <a@gkaz.ru>
;;
;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; This module contains a generic Azbook authentication backend.


;;; Code:

(define-module (azbook manager backend backend)
  #:use-module (oop goops)
  #:use-module (azbook core common)
  #:use-module (azbook core config)
  #:export (<backend>
            backend?
	          backend-config
	          backend-login
	          backend-logout))


(define-class <backend> ()
  ;; <config>
  (config
   #:init-value   #f
   #:init-keyword #:config
   #:getter       backend-config))


(define (backend? x)
  (is-a? x <backend>))



(define-method (%display (backend <backend>) (port <port>))
  (format port "#<backend ~a>"
          (object-address->hex-string backend)))

(define-method (display (backend <backend>) (port <port>))
  (%display backend port))

(define-method (write (backend <backend>) (port <port>))
  (%display backend port))


(define-method (backend-login (backend <backend>)
			                        (login <string>)
			                        (password <string>))
  (error "Not implemented"))

(define-method (backend-logout (backend <backend>))
  (error "Not implemented"))

;;; backend.scm ends here.

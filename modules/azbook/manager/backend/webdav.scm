(define-module (azbook manager backend webdav)
  #:use-modules (oop goops)
  #:export (<backend:webdav>))


(define %cookie-jar-file-prefix "/etc/azbookd.d/cookie-")


(define-class <backend:webdav> (<backend>))

(define-method (backend-login (backend <backend:webdav>)
			      (login <string>)
			      (password <string>))
  (config-set! (backend-config backend)
               "user_name"
               login))

(define-method (backend-logout (backend <backend:webdav>))
  )

;;; webdav.scm ends here.

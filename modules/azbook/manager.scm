;;; manager.scm -- Azbook manager.

;; Copyright (C) 2022 "AZ Company Group" LLC <https://gkaz.ru/>
;; Copyright (C) 2022 Artyom V. Poptsov <a@gkaz.ru>
;;
;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; This module contains implementation of Azbook manager process.


;;; Code:

(define-module (azbook manager)
  #:use-module (oop goops)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 regex)
  #:use-module (azbook manager backend nextcloud)
  #:use-module (azbook manager session)
  #:use-module (azbook core common)
  #:use-module (azbook core fqn)
  #:use-module (azbook core config)
  #:use-module (azbook core log)
  #:export (<manager>
            manager?
            make-manager
            manager-sessions
            manager-sessions-list
	    manager-users-list
            manager-find-local-user
            manager-config
            manager-authenticate!
            manager-create-user!
            manager-delete-user!
            manager-session-start!
            manager-session-stop!
            manager-session-configuration-directory
            manager-session-active?
            manager-generate-user-name
            local-user-name?))



(define %azbook-config-directory "/etc/azbook.d/")


(define-class <manager> ()
  ;; A hash table that stores currently running user sessions. For each session
  ;; the key is the login in a format "user@realm", and the value is reference
  ;; to a <session> object instance.
  ;;
  ;; <hash-table>
  (sessions
   #:init-thunk   (lambda () (make-hash-table))
   #:getter       manager-sessions)

  ;; Each Azbook user is mapped to a local auto-generated user name.
  ;;
  ;; <hash-table>
  (local-users
   #:init-thunk   (lambda () (make-hash-table))
   #:getter       manager-local-users)

  ;; The directory that stores configurations for user sessons.
  ;;
  ;; <string>
  (session-config-directory
   #:init-value   %azbook-config-directory
   #:init-keyword #:session-config-directory
   #:getter       manager-session-config-directory)

  ;; Session configuration.
  ;;
  ;; <config>
  (config
   #:init-value   #f
   #:init-keyword #:config
   #:getter       manager-config))


(define (make-manager config-file)
  "Make an Azbook manager with the specified CONFIG-FILE.  Return a
new <manager> instance."
  (make <manager> #:config-file config-file))

(define (manager? x)
  "Check if X is a <manager> instance."
  (is-a? x <manager>))



(define-method (%display (manager <manager>) (port <port>))
  (format port "#<manager ~a>"
          (object-address->hex-string manager)))

(define-method (display (manager <manager>) (port <port>))
  (%display manager port))

(define-method (write (manager <manager>) (port <port>))
  (%display manager port))


(define-method (initialize (manager <manager>) initargs)
  (next-method)
  (let ((file-name (and (memq #:config-file initargs)
                        (cadr (memq #:config-file initargs)))))
    (log-info "initialize: Configuration file: ~a" file-name)
    (slot-set! manager 'config (make <config> #:file-name file-name)))

  (unless (file-exists? (manager-session-config-directory manager))
    (log-info "initialize: Creating ~a ..."
              (manager-session-config-directory manager))
    (umask #o022)
    (mkdir (manager-session-config-directory manager))
    (umask #o066)
    (log-info "initialize: Creating ~a ... done"
              (manager-session-config-directory manager)))

  (manager-load-users manager))


(define-method (manager-session-active? (manager <manager>)
                                        (user-name <string>))
  "Predicate that checks if a session is active for the specified
USER-NAME."
  (log-debug "manager-session-active?: ~a" user-name)
  (if (hash-ref (manager-sessions manager)
                user-name)
      #t
      #f))

(define-method (manager-find-local-user (manager <manager>)
                                        (login   <string>))
  "Find a local Azbook user by their LOGIN.  Return an <fqn> instance
or #f if no user with the LOGIN is found."
  (let ((lu (manager-local-users manager)))
    (hash-fold (lambda (k v p)
                 (if (and (not p)
                          (string=? v login))
                     (string->fqn k)
                     #f))
               #f
               lu)))

(define-method (local-user-name? (login <string>))
  "Predicate that checks if a LOGIN matches the Azbook local login
pattern."
  (if (string-match "^u[a-z0-9]+$" login)
      #t
      #f))


(define-method (manager-authenticate! (manager <manager>)
                                      (login   <string>)
                                      (password <string>))
  "Authenticate a user with the specified LOGIN and PASSWORD.  LOGIN
can be either a Fully-Qualified Name (FQN) string or a local Azbook
user name that matches specific pattern.  Return #t on success, #f
otherwise."
  (log-debug "manager-authenticate!: '~a'" login)
  (let* ((user-config (config-copy (manager-config manager)))
         (fqn         (cond
		       ((fqn-valid? login)
                        (log-info "manager-authenticate!: FQN found: ~a"
                                  login)
                        (string->fqn login))
		       ((local-user-name? login)
			(manager-find-local-user manager login))
		       (else
			#f))))
    (if (not fqn)
	(begin
	  (log-info "Could not find user: ~a" login)
	  #f)
	(let* ((user-name   (fqn-login fqn))
               (user-realm  (fqn-realm fqn))
               (fqn-string  (fqn->string fqn))
	       (session (make <session>
			  #:config        user-config
                          #:fqn           fqn
			  #:user-password password)))
	  (log-info "manager-authenticate!: User name: ~a" user-name)
	  (log-info "manager-authenticate!: User realm: ~a" user-realm)
	  (if (session-authenticate session)
	      (begin

		(unless (hash-ref (manager-local-users manager) fqn-string)
		  (log-debug "manager-authenticate!: Creating user ~a ..." fqn)
		  (manager-create-user! manager fqn)
		  (log-debug "manager-authenticate!: Creating user ~a ... done" fqn))

		(let* ((local-users     (manager-local-users manager))
		       (local-user-name (hash-ref local-users fqn-string)))
		  (log-debug "Setting local user name '~a' for session '~a'"
			     local-user-name
			     session)
		  (session-local-user-login-set! session
						 local-user-name)

		  (log-debug "manager-authenticate!: Saving configuration for user ~a (~a) ..."
			     fqn-string
			     login)
		  (session-save-config! session
					(format #f "~a/~a"
						(manager-session-config-directory manager)
						local-user-name)))
		(log-debug "manager-authenticate!: Saving configuration for user ~a (~a) ... done"
			   fqn-string
			   login)
		(unless (manager-session-active? manager fqn-string)
		  (log-debug "manager-authenticate!: Storing session for a user ~a (~a) ..."
			     fqn-string
			     login)
		  (hash-set! (manager-sessions manager)
			     fqn-string
			     session)
		  (log-debug "manager-authenticate!: Storing session for a user ~a (~a) ... done"
			     fqn-string
			     login))
		#t)
	      #f)))))

(define-method (manager-sessions-list (manager <manager>))
  "Get a list of user sessions known by a MANAGER.  Return a list of
sessions."
  (log-info "manager-sessions-list: Preparing sessions list ...")
  (hash-fold (lambda (key value prev)
               (cons key prev))
             '()
             (manager-sessions manager)))

(define-method (manager-users-list (manager <manager>))
  "Get a list of users known by a MANAGER.  Return a list of users."
  (log-info "manager-sessions-list: Preparing users list ...")
  (hash-fold (lambda (key value prev)
               (cons (string-append key ":" value) prev))
             '()
             (manager-local-users manager)))

(define-method (manager-session-start! (manager <manager>)
                                       (login   <string>))
  "Start a session specified by a LOGIN.  Return #t on success, #f
otherwise."
  (log-info "manager-session-start!: Starting session for '~a'"
            login)
  (let ((session (hash-ref (manager-sessions manager)
                           login)))
    (if session
        (begin
          (log-info "manager-session-start!: Session for '~a' is found"
                    login)
          (session-start! session)
          #t)
        (begin
          (log-error "Could not find a session: ~a" login)
          #f))))

(define-method (manager-session-stop! (manager <manager>)
                                      (login   <string>))
  "Stop a session specified by a LOGIN.  Return #t on success, #f
otherwise."
  (log-info "manager-session-stop!: Stopping session for '~a'"
            login)
  (let ((session (hash-ref (manager-sessions manager)
                           login)))
    (if session
        (begin
          (log-info "manager-session-stop!: Removing '~a' from the hash table"
                    login)
          (hash-remove! (manager-sessions manager) login)
          (log-info "manager-session-stop!: Stopping '~a' session ..."
                    login)
          (session-stop! session)
          (log-info "manager-session-stop!: Stopping '~a' session ... done"
                    login)
          #t)
        (begin
          (log-error "Cloud not find a session: ~a" login)
          #f))))


(define-method (manager-generate-user-name (manager <manager>))
  "Generate an unique local user name in the format 'uXXXXXXXXXX',
where 'XXX..' is a random HEX string."
  (format #f "u~10,'0x" (random 1000000000)))


(define-method (manager-load-users (manager <manager>))
  "Load local Azbook users from a config file.  Return value is
undefined."
  (let ((users-file (string-append %azbook-config-directory "users")))
    (log-info "manager-load-users: Local Azbook users file '~a'"
	      users-file)
    (when (file-exists? users-file)
      (log-info "manager-load-users: Loading local Azbook users from '~a'"
		users-file)
      (let loop ((port (open-input-file users-file)))
	(let ((line (read-line port)))
	  (if (eof-object? line)
	      (begin
		(close port)
		(log-info "manager-load-users: ~a known users"
			  (hash-count (const #t)
				      (manager-local-users manager))))
	      (let* ((lst             (string-split line #\:))
		     (login           (car lst))
		     (local-user-name (cadr lst)))
		(hash-set! (manager-local-users manager)
			   login
			   local-user-name)
		(loop port))))))))

(define-method (manager-store-users (manager <manager>))
  "Store Azbook users to a local config file.  Return value is
undefined."
  (log-info "manager-load-users: ~a" "begin")
  (let* ((users-file (string-append %azbook-config-directory "users"))
	 (port       (open-file users-file "w")))
    (log-info "manager-load-users: Storing local Azbook users to '~a' ..."
	      users-file)
    (hash-for-each (lambda (k v)
		     (log-info "manager-load-users:   Storing '~a' ..." v)
		     (format port "~a:~a~%" k v))
		   (manager-local-users manager))
    (sync)
    (close port)
    (log-info "manager-load-users: Storing local Azbook users to '~a' ... done"
	      users-file)))

(define-method (manager-add-local-user! (manager <manager>)
                                        (fqn         <fqn>)
					(local-login <string>))
  "Add a local Azbook user with the specified FQN (Fully-Qualified
Name.)  Store the users mapping to the config file.  Return value is
undefined."
  (log-info "manager-add-local-user!: Adding  '~a' (~a) ..."
            (fqn->string fqn)
	    local-login)
  (hash-set! (manager-local-users manager)
	     (fqn->string fqn)
	     local-login)
  (manager-store-users manager))


(define-method (manager-create-user! (manager <manager>)
                                     (fqn     <fqn>))
  "Create a local user for a given FQN (Fully-Qualified Name.)"
  (let ((local-login (manager-generate-user-name manager)))
    (log-info "manager-create-user!: ~a (~a)" fqn local-login)
    (catch 'misc-error
	   (lambda ()
	     (getpwnam local-login))
	   (lambda (key . args)
	     (let ((command (format #f
				    "useradd -c '~a (Azbook user)' -m '~a'"
                                    (fqn->string fqn)
				    local-login)))
	       (unless (zero? (system command))
		 (log-error "Could not create a user: ~a" fqn))
	       (manager-add-local-user! manager fqn local-login)
	       (sync))))

    (catch 'misc-error
	   (lambda ()
	     (getpwnam local-login))
	   (lambda (key . args)
	     #f))))

(define-method (manager-delete-user! (manager <manager>)
                                     (fqn-string <string>))
  "Remove a local Azbook user.  Return #t on success, #f otherwise."
  (if (fqn-valid? fqn-string)
      (let* ((local-users (manager-local-users manager))
             (local-login (hash-ref local-users fqn-string)))
        (log-info "manager-delete-user!: local-login: ~a"
                  local-login)
        (if local-login
            (let ((command (string-join (list
                                         "userdel"
                                         "-r"
                                         (string-append
                                          "'" local-login "'")
                                         " "))))
              (log-info "Removing a local Azbook user: ~a (~a) ..."
                        local-login
                        fqn-string)
              (system command)
              (log-info "Removing a local Azbook user: ~a (~a) ... done"
                        local-login
                        fqn-string)
              (log-info "Removing the ~a from the hash ..."
                        fqn-string)
              (hash-remove! local-users fqn-string)
              (log-info "Removing the ~a from the hash ... done"
                        fqn-string)
              #t)
            (begin
              (log-info "User not found: ~a" fqn-string)
              #f)))
      (begin
        (log-error "Invalid FQN: ~a" fqn-string)
        #f)))

;;; manager.scm ends here.

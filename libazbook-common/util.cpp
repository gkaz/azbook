#include <string.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <thread>
#include <time.h>
#include <stddef.h>
#include <libgen.h>
#include <iostream>

#include "util.h"

using namespace std;

/**
 * Check if a file PATH is a directory.
 *
 * @param path -- a path to a file to check.
 * @return true if the path points to a directory,
 *   false otherwise.
 */
bool is_directory_p(const string& path)
{
     struct stat path_stat;
     stat(path.c_str(), &path_stat);
     return S_ISDIR(path_stat.st_mode);
}

/**
 * Check if a file PATH is a special file.
 *
 * @param path -- a path to a file to check.
 * @return true if the path points to a special file,
 *   false otherwise.
 */
bool is_special_file_p(const string& path)
{
    struct stat path_stat;
    stat(path.c_str(), &path_stat);
    return S_ISSOCK(path_stat.st_mode)
        || S_ISBLK(path_stat.st_mode)
        || S_ISCHR(path_stat.st_mode);
}

/**
 * Predicate.  Check if a @p d_name is the current or parent directory name.
 *
 * @param d_name A directory name to check.
 * @return @c true if @p d_name is the current or partend directory,
 *     @c false otherwise.
 */
bool is_current_or_parent_dir_p(const char* d_name)
{
     return (! strcmp(d_name, ".")) || (! strcmp(d_name, ".."));
}

/**
 * Get system load average value for the last 15 minutes.
 *
 * @return Load average as double value.
 */
double get_load_avg()
{
     static const uint32_t NELEM = 3;
     double loadavg[3];
     int numcpu = thread::hardware_concurrency();
     getloadavg(loadavg, NELEM);
     return loadavg[2] / numcpu;
}

/**
 * Get the file size.
 *
 * @param file_name A file name.
 * @return Size of @p file_name.
 */
size_t get_file_size(const char* file_name)
{
  struct stat st;
  stat(file_name, &st);
  return st.st_size;
}

/**
 * WebDAV time format.
 */
static const char WEBDAV_TIME_FMT[] = "%a, %d %b %Y %T %Z";

/**
 * Compare two modification timestamps (@p ts1 and @p ts2).
 *
 * @param ts1 The first mtime timestamp in WEBDAV_TIME_FMT.
 * @param ts1 The second mtime timestamp in WEBDAV_TIME_FMT.
 * @return -1 if ts1 is newer than ts2, 1 if ts2 is newer than ts1,
 *   and 0 if timestamps are the same.
 * @throw string Description of the error.
 */
int compare_mtimes(const string& ts1, const string& ts2)
{
     if (ts1 == ts2) {
	  return 0;
     } else {
	  struct tm ts1tm;
	  struct tm ts2tm;
	  const char* ts1c = ts1.c_str();
	  const char* ts2c = ts2.c_str();
	  memset(&ts1tm, 0, sizeof(ts1tm));
	  memset(&ts2tm, 0, sizeof(ts2tm));
	  // Tue, 01 Jan 2019 02:33:25 GMT
	  if ((! strptime(ts1c, WEBDAV_TIME_FMT, &ts1tm))
	      || (! strptime(ts2c, WEBDAV_TIME_FMT, &ts2tm))) {
	       throw "Unable to convert date and time to a timestamp";
	  }
	  time_t ts1_time = mktime(&ts1tm);
	  time_t ts2_time = mktime(&ts2tm);
	  if (ts1_time == (time_t) -1) {
	       throw "Reading time from ts1 failed";
	  }
	  if (ts2_time == (time_t) -1) {
	       throw "Reading time from ts2 failed";
	  }

	  if (ts1_time > ts2_time)
	       return -1;
	  else if (ts2_time > ts1_time)
	       return 1;
	  else
	       return 0;
     }
}

/**
 * Recursive version of 'mkdir'.
 *
 * @param path A path to make.
 * @param mode A path mode to use.
 * @return 0 on succes, non-zero value otherwise.
 */
int mkpath(const string& path, mode_t mode)
{
     if ((path.length() == 1) && (path[0] == '/'))
         return 0;

     mkpath(path.substr(0, path.rfind('/')), mode);
     return mkdir(path.c_str(), mode);
}

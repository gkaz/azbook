#ifndef DSV_H
#define DSV_H

#include <map>
#include <string>
#include <stdexcept>

class DSV_exception : public std::runtime_error {
public:
     DSV_exception(const std::string& what)
       : std::runtime_error(what) {
	  /* Do noting. */
     }
};

class DSV
{
public:
    DSV(std::string file_name);
    DSV(const DSV& dsv);
    void put(const std::string& key, const std::string& value);
    void remove(const std::string& key);
    const std::string get(const std::string key) const;
    bool contains_p(const std::string key) const;
    const std::string get_file_name() const;
    void set_file_name(const std::string& file_name);
    void save();

private:
    std::string file_name;
    std::map<std::string, std::string> data;
};

#endif // DSV_H

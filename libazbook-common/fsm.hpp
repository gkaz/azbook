/* fsm.hpp -- Finite-state machine.
 *
 * Copyright (C) 2020 "AZ Company Group" LLC <https://gkaz.ru/>
 * Copyright (C) 2020 Artyom V. Poptsov <a@gkaz.ru>
 *
 * This file is part of Azbook.
 *
 * Azbook is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Azbook is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Azbook.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __FSM_HPP__
#define __FSM_HPP__

#include <stdlib.h>
#include <map>
#include <functional>

namespace fsm {

    typedef unsigned int state_t;

    typedef struct {
        state_t next_state;
        int result;
    } result_t;

    using callback_t = std::function<result_t(void*, size_t)>;

    class FSM {
    public:
        FSM(state_t initial_state);

        /**
         * Add a new state to the finite-state machine.
         *
         * @param state -- FSM state to add callback to. If the state is not
         * present in the transition table, it will be added along with the
         * callback; if no such state is present the new callback will be
         * assigned to it.
         *
         * @param callback -- State callback. The callback must return an
         * instance of result_t structure with the next FSM state.
         */
        void add_state(state_t state, callback_t callback);

        /**
         * Execute one step of the finite-state machine.
         *
         * @param data -- the data that must be passed to the state callback.
         * @param size -- size of the data.
         * @return int as a result.
         */
        int run(void* data, size_t size);

    private:
        /**
         * The current state of the FSM.
         */
        state_t m_state;

        /**
         * Finite-state machine transition table represented by a hash map. The
         * map key is the state number, the value is a procedure that must be
         * called by the FSM while it is in this state.
         */
        std::map<state_t, callback_t> m_transition_table;

    };

}

#endif  // ifndef __FSM_HPP__

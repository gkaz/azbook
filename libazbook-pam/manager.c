#include <dbus/dbus.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdio.h>

/* PAM */
#include <security/pam_modules.h>
#include <security/pam_ext.h>
#include <security/pam_appl.h>
#include <security/_pam_macros.h>
#include <syslog.h>

const char* DBUS_MANAGER_NAME = "ru.gkaz.Azbook.Manager";
const char* DBUS_MANAGER_PATH = "/ru/gkaz/Azbook/Manager";
const char* DBUS_METHOD_AUTH          = "Authenticate";
const char* DBUS_METHOD_START_SESSION = "StartSession";
const char* DBUS_METHOD_END_SESSION   = "EndSession";

static DBusConnection* _make_dbus_connection(pam_handle_t *pamh)
{
     DBusError err;
     DBusConnection* conn = NULL;
     int result;
     dbus_error_init(&err);
     conn = dbus_bus_get(DBUS_BUS_SYSTEM, &err);
     if (dbus_error_is_set(&err)) {
          pam_syslog(pamh, LOG_ERR, "DBus connection error (%s)\n", err.message);
          dbus_error_free(&err);
          return NULL;
     }

     result = dbus_bus_request_name(conn, DBUS_MANAGER_NAME,
                                    DBUS_NAME_FLAG_REPLACE_EXISTING,
                                    &err);
     if (dbus_error_is_set(&err)) {
          pam_syslog(pamh, LOG_ERR, "DBus name error error (%s)\n", err.message);
          dbus_error_free(&err);
          return NULL;
     }

     return conn;
}

/**
 * Make a new DBus signal.
 *
 * @return A new DBus message or NULL on error.
 */
static DBusMessage* _make_start_session_call()
{
     return dbus_message_new_method_call("ru.gkaz.Azbook.Manager",
                                         DBUS_MANAGER_PATH,
                                         DBUS_MANAGER_NAME,
                                         DBUS_METHOD_START_SESSION);
}

static DBusMessage* _make_authentication_call()
{
    return dbus_message_new_method_call("ru.gkaz.Azbook.Manager",
                                        DBUS_MANAGER_PATH,
                                        DBUS_MANAGER_NAME,
                                        DBUS_METHOD_AUTH);
}

static DBusMessage* _make_end_session_call()
{
     return dbus_message_new_method_call("ru.gkaz.Azbook.Manager",
                                         DBUS_MANAGER_PATH,
                                         DBUS_MANAGER_NAME,
                                         DBUS_METHOD_END_SESSION);
}

static bool _append_login(pam_handle_t *pamh,
                          DBusMessageIter* args,
                          const char* login)
{
    pam_syslog(pamh, LOG_INFO, "login: %s", login);
    if (dbus_message_iter_append_basic(args, DBUS_TYPE_STRING, &login)) {
        return true;
    } else {
        return false;
    }
}

static bool _append_credentials(pam_handle_t *pamh,
                                DBusMessageIter* args,
                                const char* login,
                                const char* password)
{
     pam_syslog(pamh, LOG_INFO, "login: %s", login);
     if (dbus_message_iter_append_basic(args, DBUS_TYPE_STRING, &login)
         && dbus_message_iter_append_basic(args, DBUS_TYPE_STRING, &password)) {
          return true;
     } else {
          return false;
     }
}

static bool _dbus_call_method(pam_handle_t *pamh,
                              DBusConnection* conn,
                              DBusMessage* msg,
                              dbus_int32_t* response)
{
     DBusPendingCall* pending = NULL;
     DBusMessage* reply_msg;
     DBusMessageIter reply_args;
     if (! dbus_connection_send_with_reply(conn, msg, &pending, DBUS_TIMEOUT_INFINITE)) {
          pam_syslog(pamh, LOG_ERR, "Out of memory.\n");
          return false;
     }

     if (pending == NULL) {
          pam_syslog(pamh, LOG_ERR, "Pending call is NULL.\n");
          return false;
     }

     dbus_connection_flush(conn);
     dbus_pending_call_block(pending);

     reply_msg = dbus_pending_call_steal_reply(pending);
     if (reply_msg == NULL) {
          pam_syslog(pamh, LOG_ERR, "Reply is NULL.\n");
          return false;
     }

     dbus_pending_call_unref(pending);

     if (! dbus_message_iter_init(reply_msg, &reply_args)) {
          pam_syslog(pamh, LOG_ERR, "Message has no arguments.\n");
          return false;
     }

     int type = dbus_message_iter_get_arg_type(&reply_args);
     pam_syslog(pamh, LOG_ERR, "response type: %d (%c)", type, type);
     if (type == DBUS_TYPE_STRING) {
          char* error_message = "";
          dbus_message_iter_get_basic(&reply_args, &error_message);
          pam_syslog(pamh, LOG_ERR, "ERROR: %s", error_message);
          return false;
     }

     dbus_message_iter_get_basic(&reply_args, response);
     pam_syslog(pamh, LOG_ERR, "response: %d", *response);

     dbus_message_unref(reply_msg);

     return true;
}


/**
 * Try to authenticate with a WebDAV server.
 */
int32_t manager_authenticate(pam_handle_t *pamh,
			     const char* login,
			     const char* password)
{
    DBusMessage* msg = NULL;
    DBusMessageIter args;
    DBusConnection* conn = NULL;
    dbus_int32_t result = false;

    pam_syslog(pamh, LOG_INFO, "login: %s", login);

    pam_syslog(pamh, LOG_INFO, "Creating new method call...");
    msg = _make_authentication_call();
    if (msg == NULL)
        goto session_error;
     pam_syslog(pamh, LOG_INFO, "Creating new method call... done");

     pam_syslog(pamh, LOG_INFO, "Initializing arguments...");
     dbus_message_iter_init_append(msg, &args);
     pam_syslog(pamh, LOG_INFO, "Initializing arguments... done");
     pam_syslog(pamh, LOG_INFO, "Appending arguments to the method...");
     if (! _append_credentials(pamh, &args, login, password)) {
          pam_syslog(pamh, LOG_ERR, "Out of memory.\n");
          goto message_error;
     }
     pam_syslog(pamh, LOG_INFO, "Appending arguments to the method... done");
     pam_syslog(pamh, LOG_INFO, "Making a DBus connection...");
     conn = _make_dbus_connection(pamh);
     if (! conn) {
          pam_syslog(pamh, LOG_ERR, "Could not create a DBus connection.\n");
          goto message_error;
     }
     pam_syslog(pamh, LOG_INFO, "Making a DBus connection... done");
     pam_syslog(pamh, LOG_INFO, "Calling a method...");
     if (! _dbus_call_method(pamh, conn, msg, &result)) {
          pam_syslog(pamh, LOG_ERR, "Failed to call 'StartSession' method");
     }
     pam_syslog(pamh, LOG_INFO, "Calling a method... done");

connection_error:
     dbus_connection_flush(conn);
message_error:
     dbus_message_unref(msg);
session_error:
     pam_syslog(pamh, LOG_INFO, "Result: %d", result);
     return result;
}


/**
 * Start an Azbook session for the given user.
 */
bool manager_start_session(pam_handle_t *pamh,
                           const char* login)
{
     dbus_uint32_t serial = 0;
     DBusMessage* msg = NULL;
     DBusMessageIter args;
     DBusConnection* conn = NULL;
     dbus_int32_t result = false;

     pam_syslog(pamh, LOG_INFO, "login: %s", login);

     pam_syslog(pamh, LOG_INFO, "Creating new method call...");
     msg = _make_start_session_call();
     if (msg == NULL)
          goto session_error;
     pam_syslog(pamh, LOG_INFO, "Creating new method call... done");

     pam_syslog(pamh, LOG_INFO, "Initializing arguments...");
     dbus_message_iter_init_append(msg, &args);
     pam_syslog(pamh, LOG_INFO, "Initializing arguments... done");
     pam_syslog(pamh, LOG_INFO, "Appending arguments to the method...");
     if (! _append_login(pamh, &args, login)) {
          pam_syslog(pamh, LOG_ERR, "Out of memory.\n");
          goto message_error;
     }
     pam_syslog(pamh, LOG_INFO, "Appending arguments to the method... done");
     pam_syslog(pamh, LOG_INFO, "Making a DBus connection...");
     conn = _make_dbus_connection(pamh);
     if (! conn) {
          pam_syslog(pamh, LOG_ERR, "Could not create a DBus connection.\n");
          goto message_error;
     }
     pam_syslog(pamh, LOG_INFO, "Making a DBus connection... done");
     pam_syslog(pamh, LOG_INFO, "Calling a method...");
     if (! _dbus_call_method(pamh, conn, msg, &result)) {
          pam_syslog(pamh, LOG_ERR, "Failed to call 'StartSession' method");
     }
     pam_syslog(pamh, LOG_INFO, "Calling a method... done");

connection_error:
     dbus_connection_flush(conn);
message_error:
     dbus_message_unref(msg);
session_error:
     return result;
}

bool manager_end_session(pam_handle_t *pamh,
                         const char* login)
{
     dbus_uint32_t serial = 0;
     DBusMessage* msg = NULL;
     DBusMessageIter args;
     DBusConnection* conn = NULL;
     dbus_int32_t result = false;

     pam_syslog(pamh, LOG_INFO, "login: %s", login);

     pam_syslog(pamh, LOG_INFO, "Creating new method call...");
     msg = _make_end_session_call();
     if (msg == NULL) {
          goto session_error;
     }
     pam_syslog(pamh, LOG_INFO, "Creating new method call... done");

     pam_syslog(pamh, LOG_INFO, "Initializing arguments...");
     dbus_message_iter_init_append(msg, &args);
     pam_syslog(pamh, LOG_INFO, "Initializing arguments... done");

     pam_syslog(pamh, LOG_INFO, "Appending arguments to the method...");
     if (! dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &login)) {
          pam_syslog(pamh, LOG_ERR, "Out of memory.\n");
          goto message_error;
     }
     pam_syslog(pamh, LOG_INFO, "Appending arguments to the method... done");
     pam_syslog(pamh, LOG_INFO, "Making a DBus connection...");
     conn = _make_dbus_connection(pamh);
     if (! conn) {
          pam_syslog(pamh, LOG_ERR, "Could not create a DBus connection.\n");
          goto message_error;
     }

     pam_syslog(pamh, LOG_INFO, "Making a DBus connection... done");
     pam_syslog(pamh, LOG_INFO, "Calling a method...");
     if (! _dbus_call_method(pamh, conn, msg, &result)) {
          pam_syslog(pamh, LOG_ERR, "Failed to call 'EndSession' method");
     } else {
          pam_syslog(pamh, LOG_INFO, "Calling a method... done");
     }

message_error:
     dbus_message_unref(msg);
session_error:
     return result;
}

//// manager.c ends here.

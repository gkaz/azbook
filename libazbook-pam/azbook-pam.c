#define PAM_SM_AUTH

#define PAM_SM_ACCOUNT
#define PAM_SM_SESSION
#define PAM_SM_PASSWORD

#include <security/pam_modules.h>
#include <security/pam_ext.h>
#include <security/_pam_macros.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <curl/curl.h>
#include <inttypes.h>
#include <string.h>
#include <stdio.h>
#include <syslog.h>

#include "manager.h"

static const char* AZBOOK_CRED_DATA_NAME = "azbook-credentials";

static void _cleanup(pam_handle_t *pamh, void *data, int error_status) {
    if (data) {
	free (data);
    }
}

PAM_EXTERN int pam_sm_authenticate(pam_handle_t* pamh, int flags,
                                   int argc, const char** argv) {
    int result = PAM_AUTH_ERR;
    const char* user;
    const char* password;
    int rc;

    result = pam_get_user(pamh, &user, NULL);
    if (result != PAM_SUCCESS) {
        pam_syslog(pamh, LOG_ERR, "pam_get_user: ERROR");
        goto end;
    }
    pam_syslog(pamh, LOG_DEBUG, "pam_get_user: OK");

    result = pam_get_authtok(pamh, PAM_AUTHTOK, &password, NULL);
    if (result != PAM_SUCCESS) {
        pam_syslog(pamh, LOG_ERR, "pam_get_authtok: ERROR");
        goto end;
    }
    pam_syslog(pamh, LOG_DEBUG, "pam_get_authtok: OK");

    rc = manager_authenticate(pamh, user, password);
    pam_syslog(pamh, LOG_INFO, "rc: %d", rc);
    if (rc) {
        pam_syslog(pamh, LOG_INFO, "Authentication is passed.");
	if (rc == 2) {
	  pam_syslog(pamh, LOG_INFO, "User created.");
	  pam_get_user(pamh, &user, NULL);
	  pam_get_authtok(pamh, PAM_AUTHTOK, &password, NULL);
	}
        result = PAM_SUCCESS;
    } else {
        pam_syslog(pamh, LOG_ERR, "Authentication failed.");
        result = PAM_AUTH_ERR;
    }

end:
    pam_syslog(pamh, LOG_INFO, "PAM authentication is finished with code: %d",
               result);
    int* ret_data = malloc(sizeof(int));
    *ret_data = result;
    pam_set_data(pamh,
		 AZBOOK_CRED_DATA_NAME,
		 (void *) ret_data,
		 _cleanup);
    return result;
}

PAM_EXTERN int pam_sm_setcred(pam_handle_t* pamh, int flags,
                              int argc, const char** argv)
{
    int* prev_retval = NULL;
    int retval = PAM_SUCCESS;
    pam_syslog(pamh, LOG_INFO, "pam_sm_setcred");
    pam_get_data(pamh, AZBOOK_CRED_DATA_NAME, (const void **) &prev_retval);
    if (prev_retval) {
	retval = *prev_retval;
	pam_set_data(pamh, AZBOOK_CRED_DATA_NAME, NULL, NULL);
	pam_syslog(pamh,
		   LOG_INFO,
		   "pam_sm_setcred: Recovered value: %d",
		   retval);
    }
    return retval;
}

PAM_EXTERN int pam_sm_acct_mgmt(pam_handle_t* pamh, int flags,
                                int argc, const char** argv)
{
    pam_syslog(pamh, LOG_INFO, "pam_sm_acct_mgmt");
    return PAM_SUCCESS;
}

/* PAM_EXTERN int pam_sm_chauthtok(pam_handle_t* pamh, */
/* 				int flags, */
/* 				int argc, */
/* 				const char** argv) */
/* { */
/*     pam_syslog(pamh, LOG_INFO, "pam_sm_chauthtok"); */
/*     return PAM_SUCCESS; */
/* } */

PAM_EXTERN int pam_sm_open_session(pam_handle_t* pamh, int flags,
                                   int argc, const char** argv)
{
    int result = PAM_SESSION_ERR;
    const char* user = NULL;

    result = pam_get_user(pamh, &user, NULL);
    if (result != PAM_SUCCESS) {
        pam_syslog(pamh, LOG_ERR, "pam_get_user: ERROR");
        goto end;
    }
    pam_syslog(pamh, LOG_DEBUG, "pam_get_user: OK");

    if (manager_start_session(pamh, user)) {
        pam_syslog(pamh, LOG_INFO, "Session is started");
        result = PAM_SUCCESS;
    } else {
        pam_syslog(pamh, LOG_ERR, "Session is not started");
        result = PAM_IGNORE;
    }

end:
    pam_syslog(pamh, LOG_INFO, "result: %d",
               result);
    return result;
}

PAM_EXTERN int pam_sm_close_session(pam_handle_t* pamh, int flags,
                                    int argc, const char** argv)
{
    const char* user;
    int result;
    pam_syslog(pamh, LOG_INFO, "pam_sm_close_session");
    result = pam_get_user(pamh, &user, NULL);
    if (result != PAM_SUCCESS) {
        pam_syslog(pamh, LOG_ERR, "pam_get_user: ERROR");
    }

    if (manager_end_session(pamh, user)) {
        pam_syslog(pamh, LOG_INFO, "Session is stopped.");
        result = PAM_SUCCESS;
    } else {
        pam_syslog(pamh, LOG_ERR, "Session is not stopped.");
        result = PAM_SESSION_ERR;
    }

    pam_syslog(pamh, LOG_DEBUG, "pam_get_user: OK: %s", user);
    return PAM_SUCCESS;
}

PAM_EXTERN int pam_sm_password(pam_handle_t* pamh, int flags,
                               int argc, const char** argv)
{
    return PAM_SUCCESS;
}

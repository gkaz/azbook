#include <sys/inotify.h>
#include <string.h>

#include "gtest/gtest.h"
#include "../azbookd/event-queue.h"
#include "../azbookd/events.h"
#include "../libazbook-common/common.h"

using namespace std;

static void _init_baical(IP7_Client** baical_client, IP7_Trace** baical_trace)
{
     *baical_client = P7_Create_Client(TM("/P7.Sink=Console"));
     if (baical_client == NULL) {
          throw new runtime_error("Could not initialize logging");
     }
     (*baical_client)->Share(P7_SHARED);
     *baical_trace = P7_Create_Trace(*baical_client, TM("azbookd"));
     (*baical_trace)->Register_Thread(TM("azbookd"), 0);
     (*baical_trace)->Share(P7_SHARED_TRACE);
}

static void _free_baical(IP7_Client** baical_client, IP7_Trace** baical_trace)
{
     (*baical_client)->Release();
     (*baical_trace)->Release();
}


////

TEST(Event_queue, constructor)
{
     IP7_Client* baical_client;
     IP7_Trace* baical_trace;
     _init_baical(&baical_client, &baical_trace);

     Event_queue queue;
     ASSERT_EQ(queue.size(), 0);

     _free_baical(&baical_client, &baical_trace);
}

TEST(Event_queue, enqueue_dequeue)
{
     IP7_Client* baical_client;
     IP7_Trace* baical_trace;
     _init_baical(&baical_client, &baical_trace);

     Event_queue queue;
     struct inotify_event *event
          = (struct inotify_event *) calloc(sizeof(struct inotify_event) + 1, 1);

     memset(event, sizeof(struct inotify_event), 0);
     event->wd = 1;
     strcpy(event->name, "");
     
     ASSERT_EQ(queue.size(), 0) << "before enqueue";
     queue.enqueue(event);
     ASSERT_EQ(queue.size(), 1) << "after enqueue";

     ASSERT_NE(queue.front(), (struct inotify_event*) NULL);
     ASSERT_EQ(queue.size(), 1);

     queue.dequeue(event);
     ASSERT_EQ(queue.size(), 0);

     _free_baical(&baical_client, &baical_trace);
}



#include "gtest/gtest.h"
#include "../azbookd/file-utils.h"

using namespace file_utils;

TEST(azbook_file_utils, file_size_to_int)
{
    ASSERT_EQ(file_size_to_int("1"), 1);
    ASSERT_EQ(file_size_to_int("500"), 500);
    ASSERT_EQ(file_size_to_int("1K"), 1000);
    ASSERT_EQ(file_size_to_int("123M"), 123 * 1000 * 1000);
    ASSERT_EQ(file_size_to_int("2G"), 2 * 1000 * 1000 * 1000);
}

TEST(azbook_file_utils, path_to_file_name)
{
    ASSERT_EQ(path_to_file_name(6, "/12345/path", "name"),
              "/path/name");
}

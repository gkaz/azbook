#include <sys/inotify.h>
#include <string.h>

#include "gtest/gtest.h"
#include "libazbook-common/config.h"

TEST(Config, constructor)
{
     Config conf("test_config");
}

TEST(Config, get)
{
     Config conf("test_config");
     ASSERT_TRUE(conf.get("user_name") == "user");
}

TEST(Config, get_with_substitution)
{
     Config conf("test_config");
     ASSERT_TRUE(conf.get("webdav_url") == "https://192.168.1.1/files/user/");
}

TEST(Config, get_without_substitution)
{
    Config conf("test_config");
    ASSERT_EQ(conf.get("webdav_url", false),
              "https://${webdav_server}/files/${user_name}/");
}

#include <sys/inotify.h>
#include <string.h>
#include <stdexcept>

#include "gtest/gtest.h"
#include "../libazbook-common//DSV.hpp"

TEST(DSV, constructor)
{
     DSV dsv("./test_config");
}

TEST(DSV, get)
{
     DSV dsv("./test_config");
     ASSERT_TRUE(dsv.get("user_name") == "user");
}

TEST(DSV, get_missing_value)
{
     DSV dsv("./test_config");
     try {
          dsv.get("answer");
          FAIL();
     } catch (std::out_of_range& e) {
          SUCCEED();
     }
}

TEST(DSV, put) {
     DSV dsv("./test_config");
     dsv.put("answer", "42");
     ASSERT_TRUE(dsv.get("answer") == "42");
}

TEST(DSV, get_file_name) {
     const std::string file_name = "./test_config";
     DSV dsv(file_name);
     ASSERT_TRUE(dsv.get_file_name() == file_name);
}

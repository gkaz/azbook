#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string>
#include <fcntl.h>

#include "file-utils.h"

using namespace std;

/**
 * Set a file descriptor @p fd to a non-blocking mode.
 */
void file_utils::enable_nonblocking_mode_x(int fd)
{
    int flags = fcntl(fd, F_GETFL, 0);
    fcntl(fd, F_SETFL, flags | O_NONBLOCK);
}

/**
 * Convert human-readable representation of file size to an integer
 * value.
 *
 * @param size_str Human-readable file size.
 * @return Size of a file in bytes.
 */
int file_utils::file_size_to_int(const string& size_str)
{
    int len = size_str.length();
    if (len == 1) {
        return stoi(size_str);
    }
    char m = size_str.at(len - 1);
    string num_str = size_str.substr(0, len - 1);
    int multiplier = 1;
    switch (m) {
    case 'K':
        multiplier = 1000;
        break;
    case 'M':
        multiplier = 1000 * 1000;
        break;
    case 'G':
        multiplier = 1000 * 1000 * 1000;
        break;
    default:
        return stoi(size_str);
    }
    return stoi(num_str) * multiplier;
}

/**
 * Convert a PATH and NAME to a file name.  Return the file name.
 *
 * @param path_len -- the length of the root path.
 * @param path     -- the root path.
 * @param name     -- a file name;
 */
string file_utils::path_to_file_name(int32_t path_len, string path, string name)
{
    return path.substr(path_len, path.length()) + "/" + name;
}

bool file_utils::ignored_file_p(const string& file_name) {
    return ((file_name.find('#') != string::npos)
            || (file_name.find('~') != string::npos));
}

/**
 * Open @p file using @p mode.  This is a wrapper for @c fopen procedure that
 * accepts C++ strings.
 */
FILE* file_utils::sfopen(const string& file, const string& mode)
{
    return fopen(file.c_str(), mode.c_str());
}

/**
 * Make a directory @p dir using @p mode.  This is a wrapper for @c fopen
 * procedure that accepts a C++ string.
 */
int file_utils::smkdir(const string& dir, int mode)
{
    return mkdir(dir.c_str(), mode);
}


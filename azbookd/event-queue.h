#ifndef __EVENT_QUEUE_H__
#define __EVENT_QUEUE_H__

/* Standard data types. */
#include <queue>
#include <string>
#include <set>


#include "logger-client.hpp"


////

class Event_queue : public Logger_client {
public:
     Event_queue();
     void enqueue(struct inotify_event *event);
     struct inotify_event* front();
     void dequeue(struct inotify_event* event);
     int size() const;

     uint64_t get_counter() const;

private:
     /** Event queue for storing postponed inotify events. */
     std::queue<struct inotify_event*> event_queue;

     /** Event set to ensure uniqueness of stored events. */
     std::set<std::string> event_set;

     /* Baical */
     IP7_Trace::hModule baical_trace_module;

     /* Statistics */
     uint64_t events_counter;
};

#endif	/* ifndef __EVENT_QUEUE_H__ */

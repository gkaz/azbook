#ifndef __FILE_SYNCHRONIZER_H__
#define __FILE_SYNCHRONIZER_H__

#include <libxml++/libxml++.h>

#include "logger-client.hpp"


////

#include "webdav.h"

using namespace std;

class File_synchronizer_exception : public std::runtime_error {
public:
    File_synchronizer_exception(const std::string& what)
        : std::runtime_error(what)
        {
        }
};


class File_synchronizer : public Logger_client {
public:
    File_synchronizer(const string& root_directory, WebDAV* webdav);
    void sync(const string& path, const string& file = "/");
    void sync_resource(const xmlpp::Node* node, const string& path, const string& file);
    void push(const string& path);
private:
    string root_directory;
    WebDAV* webdav;

    /* Baical */
    IP7_Trace::hModule baical_trace_module;
};

#endif	/* ifndef __FILE_SYNCHRONIZER_H__ */

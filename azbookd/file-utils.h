#ifndef __FILE_UTILS_HPP__
#define __FILE_UTILS_HPP__

#include <stdio.h>
#include <string>

namespace file_utils {
    void enable_nonblocking_mode_x(int fd);
    int file_size_to_int(const std::string& size_str);
    std::string path_to_file_name(int32_t path_len,
                                  std::string path,
                                  std::string name);
    bool ignored_file_p(const std::string& file_name);

    FILE* sfopen(const std::string& file, const std::string& mode);
    int smkdir(const std::string& dir, int mode);
}

#endif  /* ifndef __FILE_UTILS_HPP__ */

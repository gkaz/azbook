#include <iostream>

#include "common.h"
#include "event-queue.h"
#include "events.h"
#include "logger-client.hpp"

using namespace std;

/**
 * Class constructor.
 */
Event_queue::Event_queue()
    : Logger_client("EQ"),
      events_counter(0)
{
    configure_logging();
}

/**
 * Get event counter for the queue.
 *
 * @return The value of the counter.
 */
uint64_t Event_queue::get_counter() const
{
    return this->events_counter;
}

/**
 * Add an @p event in the queue if it is not already present there.
 *
 * @param event An event to add.
 */
void Event_queue::enqueue(struct inotify_event* event)
{
    string euid = events::make_event_uid(event);
    LOG_DEBUG("EUID: %s", euid.c_str());
    if (this->event_set.find(euid) == this->event_set.end()) {
        ++this->events_counter;
        this->event_set.insert(euid);
        auto new_event = events::copy(event);
        event_queue.push(new_event);
        LOG_DEBUG("Event postponed: \"%s\" (queue size: %d)",
                  event->name,
                  event_queue.size());
    } else {
        LOG_DEBUG("Event is already in the queue: \"%s\"", event->name);
    }
}

/**
 * Get the size of the queue.
 *
 * @return The size of the queue.
 */
int Event_queue::size() const
{
    return this->event_queue.size();
}

/**
 * Return the oldest event from the queue.  The event will not be
 * deleted from the queue; to delete the element use
 * @ref Event_queue::dequeue() procedure.
 *
 * @return a pointer to an event from the queue.
 */
struct inotify_event* Event_queue::front()
{
    if (! this->event_queue.empty()) {
        return this->event_queue.front();
    } else {
        return NULL;
    }
}

/**
 * Remove @p event from the queue.
 */
void Event_queue::dequeue(struct inotify_event* event)
{
    string euid = events::make_event_uid(event);
    LOG_DEBUG("Removing EUID: \"%s\"", euid.c_str());
    if (this->event_set.find(euid) != this->event_set.end()) {
        this->event_set.erase(euid);
        this->event_queue.pop();
        free(event);
    }
}

//// Event_queue.cpp ends here.

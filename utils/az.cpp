/* az-manager.cpp -- An interface to the Azbook session manager.
 *
 * Copyright (C) 2020-2022 "AZ Company Group" LLC <https://gkaz.ru/>
 * Copyright (C) 2020-2022 Artyom V. Poptsov <a@gkaz.ru>
 *
 * This file is part of Azbook.
 *
 * Azbook is free software: you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Azbook is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Azbook.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <dbus-c++/dbus.h>
#include <vector>
#include <string>
#include <memory>
#include <unistd.h>

#include "azbook-managerd/service/Manager_proxy.hpp"

class Manager_client :
    public ru::gkaz::Azbook::Manager_proxy,
    public DBus::IntrospectableProxy,
    public DBus::ObjectProxy {
public:
    Manager_client(DBus::Connection& connection, const char *path, const char *name)
        : DBus::ObjectProxy(connection, path, name) {
        // Do nothing.
    }
};

using namespace std;

DBus::BusDispatcher dispatcher;

string get_session_status(Manager_client& manager_client, string& login)
{
    return manager_client.SessionStatus(login);
}

void list_sessions(Manager_client& manager_client, bool is_verbose)
{
    vector<string> result = manager_client.ListSessions();
    for (auto& session : result) {
        cout << session;
        if (is_verbose) {
            cout << ": " << get_session_status(manager_client, session);
        }
        cout << endl;
    }
}

void list_users(Manager_client& manager_client)
{
    vector<string> result = manager_client.ListUsers();
    for (auto& user : result) {
	cout << user << endl;
    }
}

void authenticate(Manager_client& manager_client,
                  const string& login,
                  const string& password)
{
    int32_t result = manager_client.Authenticate(login, password);
    cout << result << endl;
}

void session_start(Manager_client& manager_client,
                   const string& login)
{
    int32_t result = manager_client.StartSession(login);
    cout << result << endl;
}

void session_stop(Manager_client& manager_client,
                  const string& login)
{
    int32_t result = manager_client.EndSession(login);
    cout << result << endl;
}

void delete_user(Manager_client& manager_client,
                 const string& fqn)
{
    int32_t result = manager_client.DeleteUser(fqn);
    cout << result << endl;
}

void print_help(const char* app_name)
{
    cerr << "Usage: " << app_name << " <action>" << endl
         << endl
         << "Actions:" << endl
         << "    login <login>            Try to authenticate." << endl
         << "    session-start <login>    Start a session." << endl
         << "    session-stop <login>     Stop a session." << endl
         << "    list-sessions, ls        List active sessions." << endl
         << "    userdel <login>          Delete a specified user." << endl
         << "    list-users, lu           List users." << endl;
}



// Entry point.
int main(int argc, char** argv)
{
    if (argc == 1) {
        print_help(argv[0]);
        exit(0);
    }

    DBus::default_dispatcher = &dispatcher;
    DBus::Connection connection = DBus::Connection::SystemBus();
    Manager_client manager_client(connection,
                                  "/ru/gkaz/Azbook/Manager",
                                  "ru.gkaz.Azbook.Manager");

    string action(argv[1]);

    try {

        if ((action == "list-sessions") || (action == "ls")) {
            bool is_verbose = false;
            if (argc == 3) {
                string arg(argv[2]);
                is_verbose = (arg == "-v");
            }

            list_sessions(manager_client, is_verbose);
        } else if ((action == "list-users") || (action == "lu")) {
            list_users(manager_client);
        } else if (action == "userdel") {
            if (argc != 3) {
                cerr << "ERROR: Missing required argument"
                     << endl
                     << endl;
                print_help(argv[0]);
                exit(1);
            }
            string fqn(argv[2]);
            delete_user(manager_client, fqn);
        } else if (action == "login") {
            if (argc != 3) {
                cerr << "ERROR: Missing required argument"
                     << endl
                     << endl;
                print_help(argv[0]);
                exit(1);
            }
            char* password = getpass("Password: ");
            string pass(password);
            string login(argv[2]);
            authenticate(manager_client, login, pass);
        } else if (action == "session-start") {
            if (argc != 3) {
                cerr << "ERROR: Missing required argument"
                     << endl
                     << endl;
                print_help(argv[0]);
                exit(1);
            }
            string login(argv[2]);
            session_start(manager_client, login);
        } else if (action == "session-stop") {
            if (argc != 3) {
                cerr << "ERROR: Missing required argument"
                     << endl
                     << endl;
                print_help(argv[0]);
                exit(1);
            }
            string login(argv[2]);
            session_stop(manager_client, login);
        }
    } catch (DBus::Error& e) {
        cerr << "ERROR: Could not execute DBus method: " << e.what() << endl;
        return 1;
    }

    return 0;
}

//// az-manager.cpp ends here.
